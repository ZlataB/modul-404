﻿namespace PingPong
{
    partial class frmPingPong
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlSpiel = new System.Windows.Forms.Panel();
            this.picSchlaegerRechts = new System.Windows.Forms.PictureBox();
            this.picBall = new System.Windows.Forms.PictureBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.tmrSpiel = new System.Windows.Forms.Timer(this.components);
            this.vsbSchlaegerRechts = new System.Windows.Forms.VScrollBar();
            this.txtPunkte = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnUP = new System.Windows.Forms.Button();
            this.btnDOWN = new System.Windows.Forms.Button();
            this.btnLEFT = new System.Windows.Forms.Button();
            this.btnRIGHT = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.grpSteuerung = new System.Windows.Forms.GroupBox();
            this.rdbSchlaeger = new System.Windows.Forms.RadioButton();
            this.rdbBall = new System.Windows.Forms.RadioButton();
            this.pnlSpiel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSchlaegerRechts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBall)).BeginInit();
            this.grpSteuerung.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSpiel
            // 
            this.pnlSpiel.BackColor = System.Drawing.Color.Turquoise;
            this.pnlSpiel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSpiel.Controls.Add(this.picSchlaegerRechts);
            this.pnlSpiel.Controls.Add(this.picBall);
            this.pnlSpiel.Location = new System.Drawing.Point(17, 16);
            this.pnlSpiel.Margin = new System.Windows.Forms.Padding(4);
            this.pnlSpiel.Name = "pnlSpiel";
            this.pnlSpiel.Size = new System.Drawing.Size(855, 522);
            this.pnlSpiel.TabIndex = 0;
            // 
            // picSchlaegerRechts
            // 
            this.picSchlaegerRechts.BackColor = System.Drawing.Color.Black;
            this.picSchlaegerRechts.Location = new System.Drawing.Point(829, 232);
            this.picSchlaegerRechts.Margin = new System.Windows.Forms.Padding(4);
            this.picSchlaegerRechts.Name = "picSchlaegerRechts";
            this.picSchlaegerRechts.Size = new System.Drawing.Size(5, 49);
            this.picSchlaegerRechts.TabIndex = 1;
            this.picSchlaegerRechts.TabStop = false;
            // 
            // picBall
            // 
            this.picBall.BackColor = System.Drawing.Color.MediumPurple;
            this.picBall.Location = new System.Drawing.Point(369, 154);
            this.picBall.Margin = new System.Windows.Forms.Padding(4);
            this.picBall.Name = "picBall";
            this.picBall.Size = new System.Drawing.Size(33, 31);
            this.picBall.TabIndex = 0;
            this.picBall.TabStop = false;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(17, 605);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(100, 28);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Spiel Starten";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // tmrSpiel
            // 
            this.tmrSpiel.Interval = 120;
            this.tmrSpiel.Tick += new System.EventHandler(this.tmrSpiel_Tick);
            // 
            // vsbSchlaegerRechts
            // 
            this.vsbSchlaegerRechts.Location = new System.Drawing.Point(883, 16);
            this.vsbSchlaegerRechts.Name = "vsbSchlaegerRechts";
            this.vsbSchlaegerRechts.Size = new System.Drawing.Size(17, 522);
            this.vsbSchlaegerRechts.TabIndex = 2;
            this.vsbSchlaegerRechts.Value = 50;
            this.vsbSchlaegerRechts.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vsbSchlaegerRechts_Scroll);
            // 
            // txtPunkte
            // 
            this.txtPunkte.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPunkte.Location = new System.Drawing.Point(119, 545);
            this.txtPunkte.Margin = new System.Windows.Forms.Padding(4);
            this.txtPunkte.Name = "txtPunkte";
            this.txtPunkte.Size = new System.Drawing.Size(132, 30);
            this.txtPunkte.TabIndex = 3;
            this.txtPunkte.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 549);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Punkte:";
            // 
            // btnUP
            // 
            this.btnUP.BackColor = System.Drawing.Color.White;
            this.btnUP.Location = new System.Drawing.Point(959, 193);
            this.btnUP.Margin = new System.Windows.Forms.Padding(4);
            this.btnUP.Name = "btnUP";
            this.btnUP.Size = new System.Drawing.Size(100, 28);
            this.btnUP.TabIndex = 5;
            this.btnUP.Tag = "ho";
            this.btnUP.Text = "UP";
            this.btnUP.UseVisualStyleBackColor = false;
            this.btnUP.Click += new System.EventHandler(this.btnUDLR_Click);
            // 
            // btnDOWN
            // 
            this.btnDOWN.BackColor = System.Drawing.Color.White;
            this.btnDOWN.Location = new System.Drawing.Point(959, 264);
            this.btnDOWN.Margin = new System.Windows.Forms.Padding(4);
            this.btnDOWN.Name = "btnDOWN";
            this.btnDOWN.Size = new System.Drawing.Size(100, 28);
            this.btnDOWN.TabIndex = 6;
            this.btnDOWN.Tag = "ru";
            this.btnDOWN.Text = "DOWN";
            this.btnDOWN.UseVisualStyleBackColor = false;
            this.btnDOWN.Click += new System.EventHandler(this.btnUDLR_Click);
            // 
            // btnLEFT
            // 
            this.btnLEFT.BackColor = System.Drawing.Color.White;
            this.btnLEFT.Location = new System.Drawing.Point(909, 229);
            this.btnLEFT.Margin = new System.Windows.Forms.Padding(4);
            this.btnLEFT.Name = "btnLEFT";
            this.btnLEFT.Size = new System.Drawing.Size(100, 28);
            this.btnLEFT.TabIndex = 7;
            this.btnLEFT.Tag = "li";
            this.btnLEFT.Text = "LEFT";
            this.btnLEFT.UseVisualStyleBackColor = false;
            this.btnLEFT.Click += new System.EventHandler(this.btnUDLR_Click);
            // 
            // btnRIGHT
            // 
            this.btnRIGHT.BackColor = System.Drawing.Color.White;
            this.btnRIGHT.Location = new System.Drawing.Point(1017, 229);
            this.btnRIGHT.Margin = new System.Windows.Forms.Padding(4);
            this.btnRIGHT.Name = "btnRIGHT";
            this.btnRIGHT.Size = new System.Drawing.Size(100, 28);
            this.btnRIGHT.TabIndex = 8;
            this.btnRIGHT.Tag = "re";
            this.btnRIGHT.Text = "RIGHT";
            this.btnRIGHT.UseVisualStyleBackColor = false;
            this.btnRIGHT.Click += new System.EventHandler(this.btnUDLR_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(261, 547);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(249, 102);
            this.label2.TabIndex = 9;
            this.label2.Text = "Tastatursteuerung:\r\nTaste\r\nH   horizontale Flugrichtung umkehren\r\nV   vertikale F" +
    "lugrichtung umkehren\r\nP   Spiel pausieren\r\nS   Spiel weiterlaufen lassen";
            // 
            // grpSteuerung
            // 
            this.grpSteuerung.Controls.Add(this.rdbSchlaeger);
            this.grpSteuerung.Controls.Add(this.rdbBall);
            this.grpSteuerung.Location = new System.Drawing.Point(909, 545);
            this.grpSteuerung.Margin = new System.Windows.Forms.Padding(4);
            this.grpSteuerung.Name = "grpSteuerung";
            this.grpSteuerung.Padding = new System.Windows.Forms.Padding(4);
            this.grpSteuerung.Size = new System.Drawing.Size(208, 97);
            this.grpSteuerung.TabIndex = 10;
            this.grpSteuerung.TabStop = false;
            this.grpSteuerung.Text = "Wahl der Steuerung";
            // 
            // rdbSchlaeger
            // 
            this.rdbSchlaeger.AutoSize = true;
            this.rdbSchlaeger.Location = new System.Drawing.Point(9, 54);
            this.rdbSchlaeger.Margin = new System.Windows.Forms.Padding(4);
            this.rdbSchlaeger.Name = "rdbSchlaeger";
            this.rdbSchlaeger.Size = new System.Drawing.Size(149, 21);
            this.rdbSchlaeger.TabIndex = 1;
            this.rdbSchlaeger.TabStop = true;
            this.rdbSchlaeger.Text = "Schlägersteuerung";
            this.rdbSchlaeger.UseVisualStyleBackColor = true;
            this.rdbSchlaeger.CheckedChanged += new System.EventHandler(this.rdbSchlaeger_CheckedChanged);
            // 
            // rdbBall
            // 
            this.rdbBall.AutoSize = true;
            this.rdbBall.Location = new System.Drawing.Point(9, 24);
            this.rdbBall.Margin = new System.Windows.Forms.Padding(4);
            this.rdbBall.Name = "rdbBall";
            this.rdbBall.Size = new System.Drawing.Size(116, 21);
            this.rdbBall.TabIndex = 0;
            this.rdbBall.TabStop = true;
            this.rdbBall.Text = "Ballsteuerung";
            this.rdbBall.UseVisualStyleBackColor = true;
            this.rdbBall.CheckedChanged += new System.EventHandler(this.rdbBall_CheckedChanged);
            // 
            // frmPingPong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1234, 675);
            this.Controls.Add(this.grpSteuerung);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnRIGHT);
            this.Controls.Add(this.btnLEFT);
            this.Controls.Add(this.btnDOWN);
            this.Controls.Add(this.btnUP);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPunkte);
            this.Controls.Add(this.vsbSchlaegerRechts);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.pnlSpiel);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmPingPong";
            this.Text = "Ping-Pong Spiel";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPingPong_KeyDown);
            this.pnlSpiel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picSchlaegerRechts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBall)).EndInit();
            this.grpSteuerung.ResumeLayout(false);
            this.grpSteuerung.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlSpiel;
        private System.Windows.Forms.PictureBox picBall;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Timer tmrSpiel;
        private System.Windows.Forms.PictureBox picSchlaegerRechts;
        private System.Windows.Forms.VScrollBar vsbSchlaegerRechts;
        private System.Windows.Forms.TextBox txtPunkte;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUP;
        private System.Windows.Forms.Button btnDOWN;
        private System.Windows.Forms.Button btnLEFT;
        private System.Windows.Forms.Button btnRIGHT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox grpSteuerung;
        private System.Windows.Forms.RadioButton rdbSchlaeger;
        private System.Windows.Forms.RadioButton rdbBall;
    }
}

